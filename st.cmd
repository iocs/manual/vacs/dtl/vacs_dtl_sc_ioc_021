#
# Module: essioc
#
require essioc

#
# Module: vac_ctrl_mks946_937b
#
require vac_ctrl_mks946_937b


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${vac_ctrl_mks946_937b_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: DTL-020:Vac-VEG-10001
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_ctrl_mks946_937b_moxa.iocsh", "DEVICENAME = DTL-020:Vac-VEG-10001, BOARD_A_SERIAL_NUMBER = 1807170621, BOARD_B_SERIAL_NUMBER = 1505060816, BOARD_C_SERIAL_NUMBER = 1505060842, IPADDR = moxa-vac-dtl-1.tn.esss.lu.se, PORT = 4002")

#
# Device: DTL-020:Vac-VGP-10000
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = DTL-020:Vac-VGP-10000, CHANNEL = A1, CONTROLLERNAME = DTL-020:Vac-VEG-10001")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = DTL-020:Vac-VGP-10000, RELAY = 1, RELAY_DESC = 'Process PLC: Atmospheric pressure'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = DTL-020:Vac-VGP-10000, RELAY = 2, RELAY_DESC = 'Process PLC: Not used'")

#
# Device: DTL-020:Vac-VGC-10000
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgc.iocsh", "DEVICENAME = DTL-020:Vac-VGC-10000, CHANNEL = B1, CONTROLLERNAME = DTL-020:Vac-VEG-10001")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = DTL-020:Vac-VGC-10000, RELAY = 1, RELAY_DESC = 'Interlock PLC: Gates Valves Interlock (MPS)'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = DTL-020:Vac-VGC-10000, RELAY = 2, RELAY_DESC = 'Process PLC:'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = DTL-020:Vac-VGC-10000, RELAY = 3, RELAY_DESC = 'Process PLC: Not used'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = DTL-020:Vac-VGC-10000, RELAY = 4, RELAY_DESC = 'FC / HV Interlock'")

#
# Device: DTL-020:Vac-VGC-50000
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgc.iocsh", "DEVICENAME = DTL-020:Vac-VGC-50000, CHANNEL = C1, CONTROLLERNAME = DTL-020:Vac-VEG-10001")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = DTL-020:Vac-VGC-50000, RELAY = 1, RELAY_DESC = 'Interlock PLC: Gates Valves Interlock (MPS)'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = DTL-020:Vac-VGC-50000, RELAY = 2, RELAY_DESC = 'Process PLC:'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = DTL-020:Vac-VGC-50000, RELAY = 3, RELAY_DESC = 'Process PLC: Not used'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = DTL-020:Vac-VGC-50000, RELAY = 4, RELAY_DESC = 'not wired'")
